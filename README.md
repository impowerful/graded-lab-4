# 540-Fall2023-Examples

Example code illustrating the use of code to control Raspberry Pi sensors & actuators.

## Getting started

To run, use `python filename.py`

## Documentation
Raspberry Pi Camera (Hardware): https://www.raspberrypi.com/documentation/accessories/camera.html#getting-started

Pi Camera2 (Python library): https://datasheets.raspberrypi.com/camera/picamera2-manual.pdf 

### Thread examples:  
```
python threads_example.py
python threads_example_interrupt.py
python threads_example_multiple_classes.py
```   
Links:  
- Check in the end (thread-safe class): https://www.pythontutorial.net/python-concurrency/python-threading-lock/  
- **How to kill a Python thread:** https://youtu.be/A97QLHAqNuw?si=q53UWIOyDmntsstf  

### Sockets examples:
The following examples are adapted from: https://www.digitalocean.com/community/tutorials/python-socket-programming-server-client
```
python 02-socket_server.py
python 02-socket_client.py
python 02-jwt.py
```

### gRPC:
References:  
**gRPC:** https://grpc.io/docs/languages/python/quickstart/  
**Proto Buffers:** https://protobuf.dev/programming-guides/proto3/  

#Installing gRPC  
`python -m pip install grpcio`

#Installing gRPC tools  
`python -m pip install grpcio-tools`

#### gRPC example:

Example from: https://github.com/grpc/grpc/tree/master/examples/python
```
#Compile .proto file
cd rpc_example
python -m grpc_tools.protoc -I./protos --python_out=. --pyi_out=. --grpc_python_out=. ./protos/helloworld.proto
python rpc_server.py
python rpc_client.py
```

### MQTT:
References:  
**Mosquitto Broker:** https://mosquitto.org  
**Paho client:** https://eclipse.dev/paho/index.php?page=clients/python/index.php

#Build the broker  
`sudo docker compose up -d`

#Install the client  
python -m pip install paho-mqtt

#Run the example
```
python publisher.py
python subscriber.py
```

### Web API + Dashboard:   <======= UPDATED ========
References: 
**Dotnet in Raspberry Pi:** https://learn.microsoft.com/en-us/dotnet/iot/deployment  
**Dotnet VS Code:** https://learn.microsoft.com/en-us/aspnet/core/tutorials/first-web-api?view=aspnetcore-7.0&tabs=visual-studio-code  
**Dockerfile best practices:** https://docs.docker.com/develop/develop-images/dockerfile_best-practices/  
**Plotly:** https://dash.plotly.com/installation  

#### Run the WeatherForecast example in docker
```
cd web_api
sudo docker compose up -d
```
Open `http://localhost:5080/swagger` in a browser

#### Install Plotly lib
```
python -m pip install dash
python -m pip install dash_daq
```

#### Run the dashboard
```
cd dashboard
python dash_example.py
```
Open `http://localhost:8050` in a browser